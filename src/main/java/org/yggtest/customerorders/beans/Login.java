package org.yggtest.customerorders.beans;

import org.apache.commons.codec.binary.Hex;
import org.yggtest.customerorders.model.User;
import org.yggtest.customerorders.service.UserService;
import java.io.IOException;
import java.io.Serializable;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@ManagedBean
@SessionScoped
public class Login implements Serializable {

    @Inject
    private FacesContext facesContext;
    private static final long serialVersionUID = 1L;

    @Inject
    private UserService userService;

    private String password;
    private String user;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String validateUsernamePassword() throws Exception {
        boolean valid = userService.validate(user, password);
        if (valid) {
            facesContext.getExternalContext().getSessionMap().put("username", user);
            return "app/index?faces-redirect=true";
        } else if (user == null || user.equals("")) {
            facesContext.addMessage("login:username", new FacesMessage("Username cannot be empty"));
            return "";
        } else if (password == null || password.equals("")) {
            facesContext.addMessage("login:password", new FacesMessage("Password cannot be empty"));
            return "";
        } else {
            facesContext.addMessage(null, new FacesMessage("Username or password is incorrect"));
            return "";
        }
    }

    public Boolean isLogged() {
        String user = (String) facesContext.getExternalContext().getSessionMap().get("username");
        if (user == null || user.equals("")) {
            return false;
        } else {
            return true;
        }
    }

    public String register() throws Exception {
        User newUser = new User();
        if (user.equals("") || user == null) {
            facesContext.addMessage("signup:username", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Username cannot be empty", "Username cannot be empty"));
            return "signup?faces-redirect=true";
        } else if (password.equals("") || password == null) {
            facesContext.addMessage("signup:password", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "Password cannot be empty", "Password cannot be empty"));
            return "signup?faces-redirect=true";
        }
        newUser.setName(user);

        final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.reset();
        messageDigest.update(password.getBytes(Charset.forName("UTF8")));
        final byte[] resultByte = messageDigest.digest();
        password = Hex.encodeHexString(resultByte);
        newUser.setPassword(password);

        boolean created = userService.register(newUser);
        if (created) {
            return "login?faces-redirect=true";
        } else {
            facesContext.addMessage("signup:username", new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    "User already exists", "User already exists"));
            return "signup?faces-redirect=true";
        }
    }

    public void logout() throws IOException {
        facesContext.getExternalContext().invalidateSession();
        facesContext.getExternalContext().redirect("../login.xhtml");
    }
}