package org.yggtest.customerorders.beans;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;

@ManagedBean(name = "odb")
@SessionScoped
public class OrderDetailsBean implements Serializable {

    @Inject
    private FacesContext facesContext;
    private static final long serialVersionUID = 1L;

    private Long orderId;

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public void saveOrderId() throws IOException {
        facesContext.getExternalContext().getSessionMap().put("OrderId", orderId);
        facesContext.getExternalContext().redirect("../app/orderdetails.xhtml");
    }
}