package org.yggtest.customerorders.data;

import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

import org.yggtest.customerorders.model.Order;
import org.yggtest.customerorders.model.Product;

@RequestScoped
public class ProductListProducer {
    @Inject
    private ProductRepository productRepository;

    private List<Product> products;

    @Produces
    @Named
    public List<Product> getProducts() {
        return products;
    }

    public void onMemberListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Product product) {
        retrieveAllProductsOrderedByName();
    }

    @PostConstruct
    public void retrieveAllProductsOrderedByName() {
        products = productRepository.findAllOrderedByName();
    }
}
