package org.yggtest.customerorders.data;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.yggtest.customerorders.model.Order;

@RequestScoped
public class OrderListProducer {

    @Inject
    private OrderRepository orderRepository;

    @Inject
    private FacesContext facesContext;

    private List<Order> orders;
    private Long orderId;

    @Produces
    @Named
    public List<Order> getOrders() {
        return orders;
    }

    public void onMemberListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Order order) {
        retrieveAllOrdersByUser();
    }

    //validate login
    public String saveOrderId() {
        String order = facesContext.getExternalContext().getRequestParameterMap().get("OrderId");
        facesContext.getExternalContext().getSessionMap().put("OrderId", Long.valueOf(order));
        return "app/orderdetails?faces-redirect=true";
    }

    @PostConstruct
    public void retrieveAllOrdersByUser() {
        orders = orderRepository.findAllByUser();
    }
}
