package org.yggtest.customerorders.data;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.yggtest.customerorders.model.Order;
import org.yggtest.customerorders.service.UserService;

@ApplicationScoped
public class OrderRepository {

    @Inject
    private EntityManager em;
    @Inject
    private FacesContext facesContext;

    @Inject
    private UserService userService;

    public Order findById(Long id) {
        return em.find(Order.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Order> findAllByUser() {
        Session session = (Session) em.getDelegate();
        Long userId = userService.getUserId((String) facesContext.getExternalContext().getSessionMap().get("username"));
        Query query = session.createQuery("from org.yggtest.customerorders.model.Order where userId = :userId  order by date");
        query.setParameter("userId", userId);
        List<Order> orderList = query.list();
        return orderList;
    }
}
