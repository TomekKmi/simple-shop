package org.yggtest.customerorders.data;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.hibernate.Query;
import org.hibernate.Session;
import org.yggtest.customerorders.model.OrderItem;

@ApplicationScoped
public class OrderItemRepository {

    @Inject
    private EntityManager em;


    public OrderItem findById(Long id) {
        return em.find(OrderItem.class, id);
    }

    public List<OrderItem> findAllByUser(Long orderId) {
        Session session = (Session) em.getDelegate();
        Query query = session.createQuery("from OrderItem where orderId = :orderId ");
        query.setParameter("orderId", orderId);
        List<OrderItem> r = query.list();
        return r;
    }
}
