package org.yggtest.customerorders.data;

import java.util.List;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.yggtest.customerorders.model.Product;

@ApplicationScoped
public class ProductRepository {

    @Inject
    private EntityManager em;

    public Product findById(Long id) {
        return em.find(Product.class, id);
    }

    @SuppressWarnings("unchecked")
    public List<Product> findAllOrderedByName() {
        Session session = (Session) em.getDelegate();
        Criteria cb = session.createCriteria(Product.class);
        cb.addOrder(org.hibernate.criterion.Order.asc("name"));
        return (List<Product>) cb.list();
    }
}
