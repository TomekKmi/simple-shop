package org.yggtest.customerorders.data;

import java.io.IOException;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import org.yggtest.customerorders.model.OrderItem;

@RequestScoped
public class OrderItemListProducer {
    @Inject
    private OrderItemRepository orderItemRepository;
    @Inject
    private FacesContext facesContext;

    private List<OrderItem> orderItems;

    @Produces
    @Named
    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void onMemberListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final OrderItem orderItem) throws IOException {
        retrieveAllOrderItemsByUser();
    }

    @PostConstruct
    public void retrieveAllOrderItemsByUser() throws IOException {
        Long orderId = (Long) facesContext.getExternalContext().getSessionMap().get("OrderId");
        orderItems = orderItemRepository.findAllByUser(Long.valueOf(orderId));
    }
}
