package org.yggtest.customerorders.controller;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.yggtest.customerorders.beans.Item;
import org.yggtest.customerorders.model.Order;
import org.yggtest.customerorders.model.OrderItem;
import org.yggtest.customerorders.service.OrderItemRegistration;
import org.yggtest.customerorders.service.OrderRegistration;
import org.yggtest.customerorders.service.UserService;

import java.sql.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Model
public class OrderController {

    @Inject
    private FacesContext facesContext;

    @Inject
    private OrderRegistration orderRegistration;

    @Inject
    OrderItemRegistration orderItemRegistration;

    @Inject
    UserService userService;
    private Order order;

    @Produces
    @Named
    public Order getOrder() {
        return order;
    }

    public void create(Cart cart) {
        try {
            String username = (String) facesContext.getExternalContext().getSessionMap().get("username");
            Long user = userService.getUserId(username);

            List<Item> items = cart.getCart();
            Set<OrderItem> ord = new HashSet<OrderItem>();
            if (items.isEmpty()) {
                facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
                        "Unable to place order! Please add products to cart first!", "Order has not been placed"));
                return;
            }
            for (Item item: items) {
                OrderItem orderItem = new OrderItem();
                orderItem.setOrder(order);
                orderItem.setPrice(item.getProduct().getAmount());
                orderItem.setProductId(item.getProduct().getId());
                orderItem.setProductName(item.getProduct().getName());
                orderItem.setQuantity(item.getQuantity());
                ord.add(orderItem);
            }
            order.setOrderItems(ord);
            order.setTotal(cart.getTotal());
            order.setDate(new Date(new java.util.Date().getTime()));
            order.setStatus("In Progress");
            order.setUserId(user);
            orderRegistration.register(order);
            for (OrderItem item : order.getOrderItems()) {
                orderItemRegistration.register(item);
            }
            facesContext.addMessage(null,
                    new FacesMessage(FacesMessage.SEVERITY_INFO, "Created!", "Order has been successfully placed"));
            initNewMember();
            cart.getCart().clear();
        } catch (Exception e) {
            String errorMessage = getRootErrorMessage(e);
            facesContext.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
                    errorMessage, "Unable to place order! Please check input values again!"));
        }
    }

    @PostConstruct
    public void initNewMember() {
        order = new Order();
    }

    private String getRootErrorMessage(Exception e) {
        String errorMessage = "Order creation unsuccessful. See server log for more information";
        if (e == null) {
            return errorMessage;
        }

        Throwable t = e;
        while (t != null) {
            errorMessage = t.getLocalizedMessage();
            t = t.getCause();
        }
        return errorMessage;
    }

}
