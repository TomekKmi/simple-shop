package org.yggtest.customerorders.controller;


import org.yggtest.customerorders.beans.Item;
import org.yggtest.customerorders.model.Product;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.List;


@ManagedBean(name = "sp")
@SessionScoped
public class Cart {

    private List<Item> cart = new ArrayList<Item>();
    private double total;


    public double getTotal() {
        total = 0;
        for (Item item : cart) {
            total = total + (item.getQuantity()*item.getProduct().getAmount());
        }
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public List<Item> getCart() {
        return cart;
    }

    public void setCart(List<Item> cart) {
        this.cart = cart;
    }

    public String addToCart(Product product) {

        for (Item item: cart) {
            if (item.getProduct().getId() == product.getId()) {
                item.setQuantity(item.getQuantity()+1);
                return "cart";
            }
        }
        Item item = new Item();
        item.setQuantity(1);
        item.setProduct(product);
        cart.add(item);
        return "cart";
    }


    public String save() {
        OrderController oc = new OrderController();
        return "index";
    }

    public void remove(Item item) {
        for (Item itemInCart: cart) {
            if (itemInCart.equals(item)) {
                cart.remove(itemInCart);
                break;
            }
        }
    }
}
