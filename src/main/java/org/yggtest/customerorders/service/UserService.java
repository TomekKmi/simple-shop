package org.yggtest.customerorders.service;

import org.apache.commons.codec.binary.Hex;
import org.hibernate.Query;
import org.hibernate.Session;
import org.yggtest.customerorders.model.User;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Logger;


@Stateless
public class UserService {

    @Inject
    private Logger log;

    @Inject
    private EntityManager em;

    @Inject
    private Event<User> userEventSrc;

    public Long getUserId(String username) {
        Session session = (Session) em.getDelegate();
        session.beginTransaction();
        Query query = session.createQuery("from org.yggtest.customerorders.model.User where name = :username");
        query.setParameter("username", username);
        User userFromDB = (User) query.uniqueResult();
        if (userFromDB != null) {
            return userFromDB.getId();
        }
        return null;
    }

    public Boolean validate(String username, String password) throws NoSuchAlgorithmException {
        if (username.equals("") || password.equals("")) {
            return false;
        }

        final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.reset();
        messageDigest.update(password.getBytes(Charset.forName("UTF8")));
        final byte[] resultByte = messageDigest.digest();
        password = new String(Hex.encodeHex(resultByte));

        Session session = (Session) em.getDelegate();
        session.beginTransaction();
        Query query = session.createQuery("from org.yggtest.customerorders.model.User where name = :username and password = :password");
        query.setParameter("username", username);
        query.setParameter("password", password);
        User userFromDB = (User) query.uniqueResult();
        if (userFromDB != null) {
            return true;
        }
        return false;
    }


    public boolean register(User user) throws Exception {
        log.info("Registering " + user.getName());
        Session session = (Session) em.getDelegate();
        session.beginTransaction();
        Query query = session.createQuery("from org.yggtest.customerorders.model.User where name = :username");
        query.setParameter("username", user.getName());
        User userFromDB = (User) query.uniqueResult();
        if (userFromDB != null) {
            return false;
        }
        session.persist(user);
        userEventSrc.fire(user);
        return true;
    }
}
