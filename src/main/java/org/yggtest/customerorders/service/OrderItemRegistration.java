package org.yggtest.customerorders.service;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.yggtest.customerorders.model.OrderItem;

@Stateless
public class OrderItemRegistration {

    @Inject
    private Logger log;

    @Inject
    private EntityManager em;

    @Inject
    private Event<OrderItem> orderItemEventSrc;

    public void register(OrderItem orderItem) throws Exception {
        log.info("Creating order of id: " + orderItem.getId());
        Session session = (Session) em.getDelegate();
        session.persist(orderItem);
        orderItemEventSrc.fire(orderItem);
    }
}
