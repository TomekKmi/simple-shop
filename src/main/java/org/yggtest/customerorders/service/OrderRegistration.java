package org.yggtest.customerorders.service;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.logging.Logger;
import org.hibernate.Session;
import org.yggtest.customerorders.model.Order;

@Stateless
public class OrderRegistration {

    @Inject
    private Logger log;

    @Inject
    private EntityManager em;

    @Inject
    private Event<Order> orderEventSrc;

    public void register(Order order) throws Exception {
        log.info("Creating order " + order.getDate());

        Session session = (Session) em.getDelegate();
        session.persist(order);

        orderEventSrc.fire(order);
    }
}
