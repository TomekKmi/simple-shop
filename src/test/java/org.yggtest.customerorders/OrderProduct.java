package org.yggtest.customerorders;


import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.yggtest.customerorders.beans.Item;
import org.yggtest.customerorders.beans.Login;
import org.yggtest.customerorders.controller.Cart;
import org.yggtest.customerorders.data.ProductListProducer;
import org.yggtest.customerorders.data.ProductRepository;
import org.yggtest.customerorders.filter.LoginFilter;
import org.yggtest.customerorders.model.Order;
import org.yggtest.customerorders.model.OrderItem;
import org.yggtest.customerorders.model.Product;
import org.yggtest.customerorders.model.User;
import org.yggtest.customerorders.service.OrderItemRegistration;
import org.yggtest.customerorders.service.OrderRegistration;
import org.yggtest.customerorders.service.UserService;
import org.yggtest.customerorders.util.Resources;

import javax.persistence.EntityManager;
import java.io.File;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import static org.jboss.arquillian.graphene.Graphene.guardHttp;
import static org.junit.Assert.assertEquals;

@RunWith(Arquillian.class)
public class OrderProduct {
    private static final String WEBAPP_SRC = "src/main/webapp";

    @Drone
    private ChromeDriver browser;

    @ArquillianResource
    private URL deploymentUrl;

    @FindBy(id = "login:username")
    private WebElement userName;

    @FindBy(id = "login:password")
    private WebElement password;

    @FindBy(id = "login:button")
    private WebElement loginButton;

    @FindBy(tagName = "h1")
    private WebElement facesMessage;

    @FindBy(id = "nav-bar:Products")
    private WebElement productsLink;

    @FindBy(tagName = "h2")
    private WebElement productsH2;

    @FindBy(id="cartForm:register")
    private WebElement addToCart;

    @FindBy(tagName = "li")
    private WebElement message;

    @Deployment(testable = false)
    public static WebArchive createDeployment() {
        File[] files = Maven.resolver().loadPomFromFile("pom.xml")
                .importRuntimeDependencies().resolve().withTransitivity().asFile();
        return ShrinkWrap.create(WebArchive.class, "simple-shop2.war")
                .addClasses(Login.class)
                .addClasses(UserService.class)
                .addClasses(User.class)
                .addClasses(Resources.class)
                .addClasses(EntityManager.class)
                .addClasses(LoginFilter.class)
                .addClasses(ProductListProducer.class)
                .addClasses(ProductRepository.class)
                .addClasses(Product.class)
                .addClasses(Order.class)
                .addClasses(OrderItem.class)
                .addClasses(Item.class)
                .addClasses(OrderItemRegistration.class)
                .addClasses(OrderRegistration.class)
                .addClasses(Cart.class)
                .addAsLibraries(files)
                .merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class)
                                .importDirectory(WEBAPP_SRC).as(GenericArchive.class),
                        "/", Filters.include(".*\\.xhtml$"))
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(
                        new StringAsset("<faces-config version=\"2.0\"/>"),
                        "faces-config.xml");
    }

    @Test
    public void order_successfully() {
        browser.get(deploymentUrl.toExternalForm() + "login.xhtml");

        userName.sendKeys("admin");
        password.sendKeys("admin");
        guardHttp(loginButton).click();
        assertEquals("Welcome admin", facesMessage.getText().trim());
        guardHttp(productsLink).click();
        assertEquals("Products", productsH2.getText().trim());
        browser.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
        browser.findElement(By.id("productlist:j_idt29:0:button")).click();
        addToCart.click();
    }
}