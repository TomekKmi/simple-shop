package org.yggtest.customerorders;

import java.io.File;
import java.net.URL;
import java.security.MessageDigest;

import org.apache.commons.codec.BinaryEncoder;
import org.apache.commons.codec.Encoder;
import org.apache.commons.codec.binary.Hex;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.drone.api.annotation.Drone;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Filters;
import org.jboss.shrinkwrap.api.GenericArchive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.asset.StringAsset;
import org.jboss.shrinkwrap.api.importer.ExplodedImporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.yggtest.customerorders.beans.Login;
import org.yggtest.customerorders.filter.LoginFilter;
import org.yggtest.customerorders.model.User;
import org.yggtest.customerorders.service.UserService;
import org.yggtest.customerorders.util.Resources;
import javax.persistence.EntityManager;
import static org.jboss.arquillian.graphene.Graphene.guardHttp;
import static org.junit.Assert.assertEquals;


@RunWith(Arquillian.class)
public class LoginScreen {
    private static final String WEBAPP_SRC = "src/main/webapp";

    @Drone
    private ChromeDriver browser;

    @ArquillianResource
    private URL deploymentUrl;

    @FindBy(id = "login:username")
    private WebElement userName;

    @FindBy(id = "login:password")
    private WebElement password;

    @FindBy(id = "login:button")
    private WebElement loginButton;

    @FindBy(tagName = "h1")
    private WebElement facesMessage;

    @Deployment(testable = false)
    public static WebArchive createDeployment() {
        File[] files = Maven.resolver().loadPomFromFile("pom.xml")
                .importRuntimeDependencies().resolve().withTransitivity().asFile();
        return ShrinkWrap.create(WebArchive.class, "simple-shop2.war")
                .addClasses(Login.class)
                .addClasses(UserService.class)
                .addClasses(User.class)
                .addClasses(Resources.class)
                .addClasses(EntityManager.class)
                .addClasses(LoginFilter.class)
                .addClasses(Hex.class)
                .addClasses(MessageDigest.class)
                .addClasses(BinaryEncoder.class)
                .addClasses(Encoder.class)
                .addAsLibraries(files)
                .merge(ShrinkWrap.create(GenericArchive.class).as(ExplodedImporter.class)
                        .importDirectory(WEBAPP_SRC).as(GenericArchive.class),
                "/", Filters.include(".*\\.xhtml$"))
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(
                        new StringAsset("<faces-config version=\"2.0\"/>"),
                        "faces-config.xml");
    }

    @Test
    public void should_login_successfully() {
        browser.get(deploymentUrl.toExternalForm() + "login.xhtml");

        userName.sendKeys("admin");
        password.sendKeys("admin");

        guardHttp(loginButton).click();

        assertEquals("Welcome admin", facesMessage.getText().trim());
    }
}