Simple shop application

A. Setting up database
1) First change settings in persistence.xml (src\main\resources\META-INF\persistence.xml) - datasource, url, user and password
2) Test-persistence.xml (src\test\resources\test-persistence.xml) also requires some changes - please check if datasource is set properly
3) Launch import.sql (src\main\resources\import.sql)

B. Launching app
1a Use mvn clean install in project directory and copy simple-shop.war from target folder to standalone/deployments folder in wildfly
1b Launch mvn package wildfly:deploy in project directory, while standalone.bat is running (to undeploy please run mvn package wildfly:undeploy)
2. Login to shop: http://localhost:8080/simple-shop/login.xhtml. You can use predefined users: admin(password: admin), test(password: test123) or create new user

C. Setting up tests
1) Set up jbossHome directory in arquillian.xml
2) Change chromeDriverBinary directory and driver (default driver in src\test\resources\chromedriver.exe is suitable for work with Windows)
3) Launch LoginScreen or OrderProduct to test application